extends Node3D


const sxipo = preload("res://blokoj/kosmosxipoj/scenoj/sxipo_fremdulo.tscn")
const sxipo_modulo = preload("res://blokoj/kosmosxipoj/skriptoj/moduloj/sxipo.gd")


var objekto # данные станции


func _ready():
	if Global.logs:
		print('=== запуск Станции')
	Global.fenestro_itinero.malplenigi_itinero()
	Global.fenestro_itinero.projekto_itineroj_uuid = ''
	Global.fenestro_stacio = self


func _on_CapKosmostacio_ready():
	# устанавливаем корабль на место 1А
	var ship = null
	ship = sxipo.instantiate()
	var sh = sxipo_modulo.new()
	sh.create_sxipo(ship, Global.direktebla_objekto[Global.realeco-2])

	ship.visible=true
	
#	ship.rotate_y(1.58)
	ship.rotate_y(deg_to_rad(-90))
	ship.position.x = ship.position.x + 24.4 # насколько въезжать в парковку
	ship.position.z = ship.position.z - 5 # в сторону от центра
	ship.position.y = ship.position.y + 4 # высота от пола
	add_child(ship,true)
	$camera.doni_observoj(ship)


func _on_CapKosmostacio_tree_exiting():
	Global.fenestro_stacio = null

