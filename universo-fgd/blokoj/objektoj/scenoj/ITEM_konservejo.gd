extends TextureRect


var item_konservejo
var def_color = Color(0.247059, 0.25098, 0.321569, 0)

var kolumnoj = [] # массив типов колонок
# True - картинка
# False - текст

var datenoj # данные, связанные с данной строкой

#func clear_color():
#	$ITEM_background.self_modulate = def_color
#	$ITEM_background2.self_modulate = def_color
#	$ITEM_background3.self_modulate = def_color

func item_color_def():
	if not item_konservejo:
		return
	if item_konservejo.konservejo_aktuala:
		item_konservejo.konservejo_aktuala.self_modulate = def_color

func _on_ITEM_konservejo_gui_input(event):
	if Input.is_action_just_pressed("left_click"):
		if event is InputEventMouseButton:
			item_color_def()
			item_konservejo.konservejo_aktuala = self
#			print('===',$"../".item_konservejo.name)
			self_modulate = Color(0.247059, 0.25098, 0.321569, 0.784314)


# добавить колонку indico - признак иконки, data - переменная
# konservejo - указатель на склад, откуда будет браться информация о текущей выделенной строке
func aldoni_kolumno(indico, data, name_kolumno, konservejo):
	var kolumno
	item_konservejo = konservejo
	kolumnoj.append(indico)
	if len(kolumnoj)>1:
		$HBox.add_child(VSeparator.new())
	if indico:
		kolumno = TextureRect.new()
		kolumno.name = name_kolumno
	else: 
		kolumno = Label.new()
		kolumno.text = data
		kolumno.name = name_kolumno
		if len(kolumnoj) == 1:
			kolumno.align = HALIGN_LEFT
		else:
			kolumno.align = HALIGN_CENTER
		kolumno.size = Vector2(152, 17)
		kolumno.size_flags_horizontal = SIZE_EXPAND_FILL
#		kolumno.clip_text = true
	$HBox.add_child(kolumno)

