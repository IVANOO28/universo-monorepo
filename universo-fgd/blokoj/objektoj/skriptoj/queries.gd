extends Node


# количество объектов для загрузки
const count_objekto = "10"


# изменяем название объекту (поменять имя кораблю)
func sxanghi_nomo_objekto(uuid, nomo, id=0):
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.push_back(id)
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($uuid:UUID, $nomo:String)'+
		' { redaktuObjekto( uuid: $uuid, '+
		' nomo: $nomo) { ' +
		' status message  objektoj { '+
		' uuid } } } ',
		'variables': {"uuid": uuid, "nomo":nomo } }})
	if Global.logs:
		print('===sxanghi_nomo_objekto=',query)
	Net.add_net_sendij(id, 'sxanghi_nomo_objekto')
	return query


# выводит имущество пользователя
# выбираем объекты по запросившему пользователю
func get_proprietajxo_objekto(id=0):
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.push_back(id)
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'query ($realeco_Id:Float) {'+
		'filteredObjektoPosedanto  (realeco_Id:$realeco_Id) '+
		'{   edges { node { uuid '+
		"    nomo { enhavo } priskribo { enhavo } "+
		"    resurso { objId nomo { enhavo } priskribo { enhavo } "+
		"     tipo { objId nomo { enhavo } }  } "+
#		добавить глубину
		'} }}}',
		'variables': {"realeco_Id": Global.realeco } }})
	# if Global.logs:
	# 	print('===get_proprietajxo_objekto=',query)
	Net.add_net_sendij(id, 'get_proprietajxo_objekto')
	return query


