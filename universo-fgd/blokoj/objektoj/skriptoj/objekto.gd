extends "res://kerno/fenestroj/tipo_a1.gd"


var objekto # описываемый объект
var eye = false # камера наведена на данный объект
var celo = false # данный объект взят в прицел
#добавить движение к объекту
#прибли́зиться alflankiĝi (alflankigxi)

# загружаем текстуры для замены
var indico_icon_eye = preload("res://blokoj/kosmo/resursoj/eye_PNG35677_64.png")
var icon_back = preload("res://blokoj/kosmo/resursoj/back_left_arrow.png")
var indico_icon_celilo = preload("res://blokoj/objektoj/resursoj/celilo.png")
var icon_celilo_forigi = preload("res://blokoj/objektoj/resursoj/celilo_forigi.png")

# перезагружаем список описаний объектоа
func set_objekto(objekt):
	objekto = objekt
	if Global.fenestro_kosmo.get_node("camera").point_of_interest == objekto:
		eye = true
	else:
		eye = false
	icon_eye()
	celo = Global.fenestro_kosmo.get_node('ui_armilo').get_set_celo(objekto)
	icon_celilo(celo)
	$VBox/body_texture/fon/grid_image/eniri.set_disabled(true)
	if objekto.objekto['resurso']['objId'] == 1:#объект станция Espero
		#проверяем как далеко от станции и если менее 400, то разрешаем войти
		var dist = Global.fenestro_kosmo.get_node("ship").position.distance_to(objekto.position)
		if dist<400:
			$VBox/body_texture/fon/grid_image/eniri.set_disabled(false)
	FillItemList()
	if not $VBox.visible:
		$VBox.size =  Vector2(450, 300)
		$VBox.set_visible(true)


# выводим описание объекта
func FillItemList():
	$"VBox/body_texture/ItemList".clear()
	var _items = get_node("VBox/body_texture/ItemList")
	_items.add_item('имя объекта ' + objekto.objekto['nomo']['enhavo'])
	_items.add_item('uuid ' + objekto.objekto['uuid'])
	if objekto.objekto['integreco']:
		_items.add_item('целостность объекта ' + String(objekto.objekto['integreco']))
	if objekto.objekto['posedantoId']:
		_items.add_item('владелец объекта ID = ' + String(objekto.objekto['posedanto']['edges'].front()['node']['posedantoUzanto']['objId']))
		_items.add_item('ник владельца = ' + objekto.objekto['posedanto']['edges'].front()['node']['posedantoUzanto']['profilo']['retnomo'])
	if Global.fenestro_kosmo:
		var distance_to = Global.fenestro_kosmo.get_node('ship').position.distance_to(objekto.position)
		_items.add_item('расстояние до объека: ' + String(int(distance_to)))
	_items.add_item('имя ноды: ' + objekto.name + ' - ' + objekto.get_child(0).name)
#	if Global.logs:
#		if objekto.objekto.get('stokejo'):
#			print('===stokejo  = ',len(objekto.objekto['stokejo']))
#		else:
#			print(' нет склада в главном объекте (возможно в модулях)')
#		# ищем склад в модулях
#		for ch in objekto.get_children():
#			if ch.get('objekto') and ch.objekto.get('stokejo'):
#				print('===stokejo modulo 2  = ',len(ch.objekto['stokejo']))
#				Global.saveFileLogs('===stokejo modulo 2  = ')
#				Global.saveFileLogs(ch.objekto['stokejo'])
#			for ch2 in ch.get_children():
#				if ch2.get('objekto') and ch2.objekto.get('stokejo'):
#					print('===stokejo modulo 3  = ',len(ch2.objekto['stokejo']))
#					Global.saveFileLogs('===stokejo modulo 3  = ')
#					Global.saveFileLogs(ch2.objekto['stokejo'])


# функция отображения иконки осмотра объекта
func icon_eye():
	pass
	if not eye:
		$VBox/body_texture/fon/grid_image/eye.texture_normal = indico_icon_eye
	else:
		$VBox/body_texture/fon/grid_image/eye.texture_normal = icon_back


# функция отображения иконки прицеливания
# indico - признак
func icon_celilo(indico_celilo):
	if indico_celilo:
		$VBox/body_texture/fon/grid_image/celilo.texture_normal = indico_icon_celilo
	else:
		$VBox/body_texture/fon/grid_image/celilo.texture_normal = icon_celilo_forigi


func _on_ItemList_focus_entered():
	fenestro_supren()


func _on_grid_image_focus_entered():
	fenestro_supren()


func _on_eye_pressed():
	if not objekto:
		return
	Global.fenestro_kosmo.get_node("camera").choose = true
	if not eye:
		Global.fenestro_kosmo.get_node("camera").set_point_of_interest(objekto, true)
	else:
		Global.fenestro_kosmo.get_node("camera").set_point_of_interest(
			Global.fenestro_kosmo.get_node("ship"), true)
	eye = !eye
	icon_eye()
	get_viewport().set_input_as_handled() 


# взять в прицел/снять с прицеливания выделенный объект
func _on_celilo_pressed():
	if not Global.fenestro_kosmo:
		# не находимся в космосе - прицеливаться нет возможности
		return false
	# если прицелины на данный объект
	var ui_armilo = Global.fenestro_kosmo.get_node('ui_armilo')
	if celo: # взять в прицел
		ui_armilo.set_celilo(objekto)
	else: # снять с прицела
		ui_armilo.forigo_tasko_celo(objekto)
	celo = !celo
	icon_celilo(celo)
	get_viewport().set_input_as_handled() 


# двигаться к объекту
#прибли́зиться alflankiĝi (alflankigxi)
func _on_alflankigxi_pressed():
	Global.fenestro_kosmo.get_node('Control').alflankigxi(objekto)


# вход в станцию
func _on_eniri_pressed():
	Global.fenestro_kosmo.get_node('Control').go_kosmostacioj(objekto)
