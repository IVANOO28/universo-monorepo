extends "res://kerno/fenestroj/tipo_a1.gd"
# индикаторы управляемого объекта


var indicoj = [] # список показателей для вывода на экран
var speed = 0 # скорость корабля


# выводим описание объекта
func FillItemList():
	var _items = get_node("VBox/body_texture/ItemList")
	_items.clear()
	_items.add_item('скорость корабля = '+String(speed))
	for item in indicoj:
		_items.add_item(item)


func _on_ItemList_focus_entered():
	fenestro_supren()

