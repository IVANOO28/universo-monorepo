extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "rajtigo"


# количество объектов для загрузки
const count_objekto = "30"
# const count_objekto = "3"

# строка запроса статуса модификации с характеристиками текущей модификации
const qyery_stato = "  stato{uuid objId potenco statoAntaua {integreco} "+\
	"   statoSekva{integreco} "+\
	"   posedanto{uuid nomo{enhavo} "+\
	"    karakterizado{edges{node{ uuid lateroKarakterizado tipo { objId nomo{enhavo} }}} } }}"

# строка запроса статуса модификации с характеристиками текущей модификации
const qyery_modifo = "  modifo{uuid "+\
	"   posedanto{ "+\
	"    karakterizado{edges{node{ uuid lateroKarakterizado tipo { objId nomo{enhavo} }}} } }}"

const query_objektostokejoSet = "  objektostokejoSet{" +\
	"   edges{node{uuid}}}" 


# запрос полей объекта
const query_objekto = "uuid integreco volumenoInterna volumenoEkstera" +\
	"  volumenoStokado      nomo{enhavo} resurso{objId} "

const query_ligilo_starto = "ligilo" # плюс параметр (tipo_Id_In:$ligiloTipo)
const query_ligilo_mezo =\
	"{edges{node{ uuid "+\
	" posedantoStokejo {uuid} konektiloPosedanto konektiloLigilo tipo{objId} "+\
	" ligilo{  "+query_objekto+\
	qyery_stato+query_objektostokejoSet
	# вложенность вставляется в это место
const query_ligilo_fino = "}}}}" 


# запрос на список управляемых объектов
func get_direktebla_json(statusoId, tipoId, id=0):
	if !id:
		id = Net.get_current_query_id_and_liberigi()
#	var statusoIdIn = String(Net.statuso_nova) + ','+String(Net.statuso_laboranta) + ','+String(Net.status_pauzo)
	var statusoIdIn = str(Net.statuso_nova) + ','+ str(Net.statuso_laboranta) + ','+ str(Net.status_pauzo)
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload': {"query": "query ($UzantoId:Int, $statusoId:Float, "+
		" $tipoId:Float, $statusoIdIn:String, $ligiloTipo:String)"+
		"{ objekto (" +
		" objektouzanto_Isnull:false,"+
		" objektouzanto_Autoro_Id:$UzantoId," +
		") { edges { node { uuid integreco volumenoInterna volumenoEkstera volumenoStokado" +
		" projekto (statuso_Id: $statusoId, tipo_Id: $tipoId){ "+
		"  edges { node { uuid "+
		"  kategorio {edges {node {objId nomo{enhavo}}}}" +
		"  tasko (statuso_Id_In:$statusoIdIn){ edges {node { "+
		"   uuid finKoordinatoX finKoordinatoY finKoordinatoZ "+
		"   objekto{uuid} " +
		"   nomo{enhavo} priskribo{enhavo} " +
		"   kategorio {edges {node {objId nomo{enhavo}}}}" +
		"   pozicio statuso {objId } } } } } } } "+
		" nomo { enhavo } priskribo { enhavo } "+
		" resurso { objId nomo { enhavo } priskribo { enhavo } "+
		"  tipo { objId nomo { enhavo } } "+
		" } "+
		" koordinatoX koordinatoY koordinatoZ "+
		' posedantoObjekto '+
		'  { uuid } '+
		qyery_stato+
		" ligiloLigilo{edges{node{ uuid "+
		"  posedanto{ kubo {objId} koordinatoX koordinatoY " +
		" koordinatoZ uuid nomo {enhavo} }}}}" +
		query_ligilo_starto + "(tipo_Id_In:$ligiloTipo)" +
		query_ligilo_mezo +
		query_ligilo_starto + "(tipo_Id_In:$ligiloTipo)" +
		query_ligilo_mezo +
		query_ligilo_fino +
		query_ligilo_fino +
		" realeco{objId}" +
		" posedanto{edges{node{" +
		"  posedantoUzanto{  objId profilo{retnomo}}}}}" +
		" rotaciaX rotaciaY rotaciaZ } } } }",
		'variables': {"statusoId":statusoId, 
		"tipoId":tipoId, "statusoIdIn":statusoIdIn,
		"UzantoId":Global.id, "ligiloTipo":"1,2"} } })
	if Global.logs:
		Global.saveFileLogs("=== get_direktebla_query = ")
		Global.saveFileLogs(query)
		# print("=== get_direktebla_query = ",query)
	return query


# запрос на восходящую связь по конкретному объекту (внутри какого объекта находится)
func get_objekto_uuid(uuid_objekto, id):
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload': {"query": "query ($uuid_objekto:UUID)"+
		"{ objekto (" +
		" uuid:$uuid_objekto" +
		") { edges { node { " +
		" ligiloLigilo{edges{node{ uuid "+
		"  posedanto{ kubo {objId} koordinatoX koordinatoY " +
		"  koordinatoZ uuid nomo {enhavo} }}}}" +
		" } } } }",
		'variables': {"uuid_objekto":uuid_objekto, 
		} } })
	if Global.logs:
		print("=== get_objekto_uuid = ",query)
	Net.add_net_sendij(id, 'get_objekto_uuid')
	return query


# запрос на список управляемых объектов в космосе
func get_direktebla_kosmo_json(id=0):
	if !id:
		id = Net.get_current_query_id_and_liberigi()
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload': { "query": "query ($UzantoId:Int )"+
	"{ filteredObjekto (" +
	" objektouzanto_Isnull:false,"+
	" objektouzanto_Autoro_Id:$UzantoId," +
	" koordinatoX_Isnull:false, koordinatoY_Isnull:false, koordinatoZ_Isnull:false," +
	" kubo_Isnull:false," +
	") { edges { node { uuid " +
	"  realeco{objId}}}}}",
	'variables': {"UzantoId":Global.id} } })
	if Global.logs:
		print("=== get_direktebla_kosmo_json = ",query)
	# print("=== get_direktebla_kosmo_query = ",query)
	return query


# Запрос к API, выбираем объекты, которые в космосе
# statusoId - статус проекта (2=в работе)
# tipoId - тип проекта Универсо (2 - Для объектов)
func get_objekto_kosmo(statusoId, tipoId, kuboId=Global.kubo, id=0, after=""):
	if !id:
		id = Net.get_current_query_id_and_liberigi()
	var statusoIdIn = [Net.statuso_nova, Net.statuso_laboranta, Net.status_pauzo]
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	# 'payload':{ "query": "query ($kuboId:Float, $statusoId:Float, "+
		# " $realecoId:Float, $tipoId:Float, $statusoIdIn: String, $ligiloTipo:String) " +
	'payload':{ "query": "query ($kuboId:Float, $statusoId:Int, "+
		" $realecoId:Float, $tipoId:Int, $statusoIdIn: [Int]) " +#, $ligiloTipo:String) " +
		"{ objektoProjektoResursoStato (realeco_Id:$realecoId, kubo_Id: $kuboId, "+
		# "{ filteredObjekto (realeco_Id:$realecoId, kubo_Id: $kuboId, "+
		" koordinatoX_Isnull:false, koordinatoY_Isnull:false, koordinatoZ_Isnull:false," +
		" first:"+count_objekto+', after: "'+after+'" ' +
		" ) { pageInfo { hasNextPage endCursor } edges { node { uuid posedantoId "+
		" integreco volumenoInterna volumenoEkstera volumenoStokado" +
		# проект стрельбы  с задачами прицеливания и ведения огня

		# проект движени
		# " projekto (statuso_Id: $statusoId, tipo_Id: $tipoId, "+
		# "   kategorio_Id:$kategorioMovado){ "+

		" taskojprojektoSet (statuso_Id: $statusoId, tipo_Id: $tipoId){ "+
		"  edges { node { uuid statuso{objId}"+
		"  kategorio {edges {node {objId nomo{enhavo}}}}" +
		"  taskojtaskoSet (statuso_Id_In: $statusoIdIn){ edges {node { "+
		"   uuid finKoordinatoX finKoordinatoY finKoordinatoZ "+
		"   objekto{uuid} " +
		"   nomo{enhavo} priskribo{enhavo} " +
		"   kategorio {edges {node {objId nomo{enhavo}}}}" +
		"   pozicio statuso {objId} "+
		" } } } } } } "+
		" nomo { enhavo } priskribo { enhavo } "+
		" resurso { objId nomo { enhavo } priskribo { enhavo } "+
		"  tipo { objId nomo { enhavo } } "+
		" } "+
		" koordinatoX koordinatoY koordinatoZ "+
		' posedantoObjekto { uuid } '+
		# query_objektostokejoSet +
		qyery_stato +
		# query_ligilo_starto + "(tipo_Id_In:$ligiloTipo)" +
		# query_ligilo_mezo +
		# query_ligilo_starto + "(tipo_Id_In:$ligiloTipo)" +
		# query_ligilo_mezo +
		# query_ligilo_fino +
		# query_ligilo_fino +
		" posedanto{edges{node{" +
		"  posedantoUzanto{  objId profilo{retnomo}}}}}" +
		" rotaciaX rotaciaY rotaciaZ } } } }",
		'variables': {"kuboId":kuboId, "statusoId":statusoId, 
		"tipoId":tipoId, "statusoIdIn":statusoIdIn,
		"realecoId":Global.realeco, "ligiloTipo":"1,2"} } })
		# задача - получать данные типа 3 с астероидов
	if Global.logs:
		Global.saveFileLogs("=== get_objekto_kosmo = ")
		Global.saveFileLogs(query)
	# 	print('===get_objekto_kosmo=')#,query)
	Net.add_net_sendij(id, 'get_objekto_kosmo')
	return query


# Запрашиваем данные конкретного объекта и его связи по частям
func get_objekto(uuid, id=0, after=""):
	if !id:
		id = Net.get_current_query_id_and_liberigi()
	var univesoObjekto = ""
	if !after:
		univesoObjekto = 	" objekto ( uuid:$uuid ) "+\
		"  { edges { node { "+\
		"    uuid posedantoId  integreco volumenoInterna volumenoEkstera "+\
		"    volumenoStokado "+\
		"    nomo { enhavo } priskribo { enhavo } "+\
		"    resurso { objId nomo { enhavo } priskribo { enhavo } "+\
		"     tipo { objId nomo { enhavo } }  } "+\
		"    koordinatoX koordinatoY koordinatoZ  "+\
		"    posedantoObjekto   { uuid }  "+\
		qyery_stato+\
		"    rotaciaX rotaciaY rotaciaZ } } } "
	
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ "query": "query ($uuid:UUID) " +#, $ligiloTipo:String) "+
	" { " + univesoObjekto +
	" objektoLigilo( "+
	"  first:"+count_objekto+', after: "'+after+'" ' + 
	"  posedanto_Uuid:$uuid) "+
	"  {pageInfo { hasNextPage endCursor } "+
	"   edges{node{ uuid   posedantoStokejo {uuid} "+
	"    konektiloPosedanto konektiloLigilo tipo{objId} tipo{objId}"+
	"    ligilo{ " + query_objekto +
	qyery_stato +
	"      posedanto{edges{node{ "+\
	"       posedantoUzanto{ objId profilo{retnomo}}}}} "+
	query_objektostokejoSet +
	# query_ligilo_starto + "(tipo_Id_In:$ligiloTipo)" +
	# query_ligilo_mezo +
	# query_ligilo_starto + "(tipo_Id_In:$ligiloTipo)" +
	# query_ligilo_mezo +
	# query_ligilo_fino +
	# query_ligilo_fino +
	"      } " +
	" }}} } ",
		'variables': {"uuid":uuid} } }) # запрашиваем только связи для сборки объектов
		# 'variables': {"uuid":uuid, "ligiloTipo":"1,2"} } }) # запрашиваем только связи для сборки объектов
		# !!!!! нужно на уровне прав заблокировать то, что находится внутри чужих кораблей (только через задачи при сканировании)
	if Global.logs:
	# 	print('===get_objekto=',query)
		Global.saveFileLogs('=== get_objekto =')
		Global.saveFileLogs(query)
	Net.add_net_sendij(id, 'get_objekto')
	return query


# отказ от подписки
func nuligo_subscribtion(id):
	var query = JSON.stringify({
		'type': 'stop',
		'id': '%s' % id})
	# if Global.logs:
	# 	print('=== nuligo_subscribtion =query= ',query)
	return query


# подписка на связь с объектом
func subscription_ligilo(ligilo, id):
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'subscription ($ligilo: String!)'+
		' { ligiloEventoj (ligilo: $ligilo) { '+
		' evento '+
		' ligilo { uuid forigo } '+
		' } } ',
		'variables': {"ligilo": ligilo } }})
	if Global.logs:
		Global.saveFileLogs('=== subscription_ligilo =')
		Global.saveFileLogs(query)
		# print('=== subscription_ligilo =query= ',query)
	return query


func subscription_objekto_ligilo(uuid_objekto, id):
	"""
	подписка на изменения в подчинённых связях (объектах) конкретного объекта
	"""
	var query = JSON.stringify({
		'type': 'start',
		'id': '%s' % id,
		'payload':{ 'query': 'subscription ($uuid_objekto:String!, $ligiloTipo:String)'+
		'{objektoLigiloEventoj(objekto:$uuid_objekto)'+
		' { evento ligilo { uuid forigo '+
		'  posedantoStokejo {uuid} '+
		'  konektiloPosedanto konektiloLigilo tipo{objId} tipo{objId} '+
		'  ligilo { ' + query_objekto +
		qyery_stato+
		'   posedanto{edges{node{ posedantoUzanto{ objId profilo{retnomo}}}}} '+
		query_objektostokejoSet +
		'   ligilo(tipo_Id_In:$ligiloTipo){edges{node{ uuid '+
		'    posedantoStokejo {uuid} konektiloPosedanto konektiloLigilo tipo{objId} '+
		'    ligilo{ '+ query_objekto +
		query_objektostokejoSet +
		'    }}}}}} '+
		' fadenoLigilo { uuid forigo '+
		'  posedantoStokejo {uuid} '+
		'  konektiloPosedanto konektiloLigilo tipo{objId} tipo{objId} '+
		'  ligilo { '+ query_objekto +
		qyery_stato+
		'   posedanto{edges{node{ posedantoUzanto{ objId profilo{retnomo}}}}} '+
		query_objektostokejoSet +
		query_ligilo_starto + "(tipo_Id_In:$ligiloTipo)" +
		query_ligilo_mezo +
		query_ligilo_starto + "(tipo_Id_In:$ligiloTipo)" +
		query_ligilo_mezo +
		query_ligilo_fino +
		query_ligilo_fino +
			' }} '+
		' objektoSxangxi{ '+
		'  uuid' +
		' } '+
		'}}',
		'variables': {"uuid_objekto":uuid_objekto,
		"ligiloTipo":"1,2" } }})
	if Global.logs:
		Global.saveFileLogs('=== subscription_objekto_ligilo =')
		Global.saveFileLogs(query)
		# print('=== subscription_objekto_ligilo =query= ',query)
	return query


func get_stokejo(uuid, id=0, after=""):
	"""
	Запрашиваем данные склада/места хранения и какие объекты он хранит
	uuid - uuid objektoStokejo
	"""
	if !id:
		id = Net.get_current_query_id_and_liberigi()
	
	var query = JSON.stringify({
		'type': 'start',
		'id': '%s' % id,
		'payload':{ "query": "query ($uuid:UUID, $ligiloTipo:String)"+
		"{  objektoStokejo ( "+
		"  uuid: $uuid ) "+
		" { edges { node { uuid "+
		"  nomo { enhavo } priskribo { enhavo } "+
		"  posedantoObjekto   { uuid } "+
		"  objekto ( "+
		"   first:"+count_objekto+', after: "'+after+'" ' + 
		"  ){ "+
		"   pageInfo { hasNextPage endCursor } "+
		"    edges { node { "+ query_objekto +
		qyery_stato +
		query_ligilo_starto + "(tipo_Id_In:$ligiloTipo)" +
		query_ligilo_mezo +
		query_ligilo_starto + "(tipo_Id_In:$ligiloTipo)" +
		query_ligilo_mezo +
		query_ligilo_fino +
		query_ligilo_fino +
		" } } }} }}} ",
		'variables': {"uuid":uuid, "ligiloTipo":"1,2"} } }) 
	if Global.logs:
		Global.saveFileLogs('=== get_stokejo =')
		Global.saveFileLogs(query)
	# 	print('===get_stokejo=',query)
	Net.add_net_sendij(id, 'get_stokejo')
	return query


# подписка на пользователя в реальности
func subscribtion_uzanto_realeco(id):
	#var statusoIdIn = '"'+String(Net.statuso_nova) + ','+String(Net.statuso_laboranta) + ','+String(Net.status_pauzo)+'"'
	#var statusoId = String(Net.statuso_laboranta)
	#var tipoId = String(Net.tasko_tipo_objekto)
	var statusoIdIn = '"'+ str(Net.statuso_nova) + ','+ str(Net.statuso_laboranta) + ','+ str(Net.status_pauzo)+'"'
	var statusoId = str(Net.statuso_laboranta)
	var tipoId = str(Net.tasko_tipo_objekto)
	var tipoLigilo = '"'+'1, 2'+'"'
	var q = Title.QueryObject.new()
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'subscription ($realeco:Int!)'+
		'{ uzantoEventoj (realeco:$realeco) { evento '+
		' objekto { uuid koordinatoX koordinatoY koordinatoZ '+
		"  nomo { enhavo } priskribo { enhavo } "+
		"  integreco volumenoInterna volumenoEkstera volumenoStokado " +
		"  kubo{objId} " +
		"  posedantoId " +
		"  posedanto{edges{node{" +
		"   posedantoUzanto{ objId profilo{retnomo}}}}}" +
		"  stato{objId potenco integreco statoAntaua {integreco} "+
		"   statoSekva{integreco}}" +
		"  resurso{objId tipo{objId}}" +
		"  ligiloLigilo{edges{node{ uuid "+
		"   tipo {objId} " +
		"   posedantoStokejo { uuid nomo{enhavo} " +
		"    priskribo { enhavo } "+
		"    posedantoObjekto   { uuid } } "+
		"   posedanto{ kubo {objId} koordinatoX koordinatoY " +
		"    koordinatoZ uuid nomo {enhavo} }}}}" +
		q.query_objektostokejoSet +
		q.qyery_stato +
		q.query_ligilo_starto + "(tipo_Id_In: "+tipoLigilo+")" +
		q.query_ligilo_mezo +
		q.query_ligilo_starto + "(tipo_Id_In: "+tipoLigilo+" )" +
		q.query_ligilo_mezo +
		q.query_ligilo_fino +
		q.query_ligilo_fino +
		"  projekto (statuso_Id: "+statusoId+", tipo_Id: "+tipoId+"){ "+
		"   edges { node { uuid statuso{objId} "+
		"   kategorio {edges {node {objId nomo{enhavo}}}}" +
		"   tasko (statuso_Id_In: "+statusoIdIn+"){ edges {node { "+
		"    uuid finKoordinatoX finKoordinatoY finKoordinatoZ "+
		"    objekto{uuid} " +
		"    nomo{enhavo} priskribo{enhavo} " +
		"    kategorio {edges {node {objId nomo{enhavo}}}}" +
		"    pozicio statuso {objId} } } } } } } "+
		'  rotaciaX rotaciaY rotaciaZ } '+
		'} }',
		'variables': {"realeco": Global.realeco} }})
	if Global.logs:
		Global.saveFileLogs('=== subscribtion_uzanto_realeco =query= ')
		Global.saveFileLogs(query)
		# print('=== subscribtion_uzanto_realeco =query= ',query)
	Net.add_net_sendij(id, 'subscribtion_uzanto_realeco')
	return query


# запрос на восходящую связь по конкретному объекту (внутри какого объекта находится)
func projekto_statuso_fermi():
	var id = Net.get_current_query_id_and_liberigi()
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload': {"query": "mutation{"+
	" redaktuFermiTaskojProjekto "+
	" {status message projektoj{ uuid }}}",
	} })
	if Global.logs:
		Global.saveFileLogs('=== projekto_statuso_fermi == ')
		Global.saveFileLogs(query)
	# 	print("=== projekto_statuso_fermi = ",query)
	Net.add_net_sendij(id, 'projekto_statuso_fermi')
	return query


