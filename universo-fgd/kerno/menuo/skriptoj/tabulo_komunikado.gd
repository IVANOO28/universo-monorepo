extends Control

# сцена сообщений - сюда выводятся все сообщения для пользователя
# tabulo - сцена
# komunikado - сообщение

var komunikadoj = [] # список сообщений
# Список состоит из словарей, структура словаря:
# tempo - сколько тиков tempulino уже показывается
# komunikado - сообщение

# tempumilo - таймер
# pasho - шаг
# сколько времени каждое сообщение на экране находится
var pasho_komunikado = 10

var temp = 1

# aldoni- добавить
func aldoni(komunikado):
	komunikadoj.append({
		'tempo': 0,
		'komunikado':str(temp) + ') ' + komunikado
	})
	temp += 1
	if $tempumilo.is_stopped():
		_on_tempumilo_timeout()
		$tempumilo.start()



func _on_tempumilo_timeout():
	$komunikado.text = ''
	var i_komunikado = 0
	var masivo_forigo = [] # массив индексов на удаление
	for komunikado in komunikadoj:
		if komunikado['tempo'] == pasho_komunikado:
			masivo_forigo.insert(0, i_komunikado) # нужно удалять после окончания цикла, а то пропускаем следующую запись?
		else:
			komunikado['tempo'] += 1
			$komunikado.text += '\n' + komunikado['komunikado']
		i_komunikado += 1
	for forigo in masivo_forigo:
		komunikadoj.remove(forigo)
	if len(komunikadoj) == 0:
		$tempumilo.stop()


