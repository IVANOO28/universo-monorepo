extends Node
#при переходе в мир на станцию не идёт подгрузка объектов

const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")

# сигнал загрузки объектов
signal load_objekto()


@onready var ui = get_node("CanvasLayer/UI/UI") # нода окон взаимодействия


var load_scene : String
var id_direktebla_query # под каким номером запроса отправили запрос на управляемый список
var id_direktebla_kosmo_query # под каким номером запроса отправили запрос на управляемый список в космосе
var id_objekto_kosmo# id запроса на объекты в космосе
var id_objekto = []# id запроса на полные данные по конкретному объекту для списка Global.objektoj
# храним структуру:
# uuid - по какому именно объекту запрос
# id - id самого запроса
var id_stokejo = []# id запроса на содержание склада/места хранения
# храним структуру:
# uuid_objekto - по какому именно объекту запрос
# uuid_modulo - в каком модуле склад
# id - id самого запроса
var id_ligilo = 0 # id запроса на   выход из станции/заход в станцию
var ligilo_subscription_id = 0 # подписка на выход из станции
var objekto_ligilo_subscription_id = 0 # подписка на объект-станцию
var id_statio = 0 # запрос на данные по станции

var subscription_uzanto_realeco_id = 0 # id сообщения (подписка uzantoEventoj)


func _ready():
	var config = ConfigFile.new()
	var err = config.load("res://settings.cfg")
	if err == OK:
		Global.server = config.get_value("global", "server", false)
		Global.autoload = config.get_value("global", "autoload", false)
		Global.logs = config.get_value("global", "logs", false)
		Global.admin = config.get_value("global", "admin", false)
		Global.projekto_statuso_fermi = config.get_value("global", "projekto_statuso_fermi", false)
		Net.url_server = config.get_value("net", "url_server", "t34.universo.pro")
		Net.ws = config.get_value("net", "ws", "wss")
		Net.http = config.get_value("net", "http", "https")
		Global.gramofono_paused = config.get_value("gramofono", "paused", false)
		if Global.admin:
			$CanvasLayer/UI/administri.set_visible(true)
	if Global.logs:
		print('запуск res://kerno/menuo/menuo.tscn')
# warning-ignore:return_value_discarded
	Net.connect("connection_failed", Callable(self, "_on_connection_failed"))
# warning-ignore:return_value_discarded
	Net.connect("connection_succeeded", Callable(self, "_on_connection_success"))
# warning-ignore:return_value_discarded
	Net.connect("server_disconnected", Callable(self, "_on_server_disconnect"))
	# подключаем сигнал для обработки входящих данных
	err = Net.connect("input_data", Callable(self, "_on_data_start"))
	if err:
		print('error = ',err)
	# выводим версию программы
	# TODO: Refactor 
	#$CanvasLayer/Label.text = 'Universo version '+String(Global.numero_versio)+'.'+\
		#String(Global.numero_subversio)+'.'+String(Global.numero_korektado)+'-alfo'
	get_tree().set_auto_accept_quit(false) # не даст закрыться программе

# TODO: Refactor 
#func _notification(what):
	#if what == NOTIFICATION_WM_QUIT_REQUEST:
		#_on_eliro_button_up()

func go_realeco():
	if Global.realeco == 2:
		on_com()
	elif Global.realeco == 1:
		on_real()
	elif Global.realeco == 3:
		on_cap()


# Обработчик сигнала "connection_succeeded"
func _on_connection_success():
	var q = QueryObject.new()
	# если запустился сервер - отправляем запрос на закрытие ненужных проектов
	if Global.projekto_statuso_fermi:
		Net.send_json(q.projekto_statuso_fermi())
	id_direktebla_query = Net.get_current_query_id()
	Net.send_json(q.get_direktebla_json(Net.statuso_laboranta, Net.tasko_tipo_objekto, id_direktebla_query))


# закрываем признаки загрузки данных
func fermo_signo(): # закрыть признак
	# если ещё загрузки не было, то и отключать нечего
	if not Global.loading:
		return
	Global.loading = false
	#отключаем перехват сигнала
	Net.disconnect("input_data", Callable(self, "_on_data"))
	Net.data_server.clear()
# warning-ignore:return_value_discarded
	Net.connect("input_data", Callable(self, "_on_data_start"))
	# снимаем признаки загрузки параллельных миров
	for obj in Global.direktebla_objekto:
		obj['kosmo'] = Global.server # server всегда в космосе или нет изначально


# Обработчик сигнала "connection_failed"
func _on_connection_failed():
	fermo_signo()
	if !Global.server:
		_on_real_pressed()
	pass


# Обработчик сигнала "server_disconnected"
func _on_server_disconnect():
	fermo_signo()
	if !Global.server:
		_on_real_pressed()
	pass

# обработчик сигнала прихода данных при старте программы
func _on_data_start():
	var i_data_server = 0
	var masivo_forigo = [] # массив индексов на удаление
	for on_data in Net.data_server:
		# ответ на запрос списка управляемый объектов
		if on_data['id'] == String(id_direktebla_query):
			print('=== on_data == ', on_data)
			if Global.logs:
				Global.saveFileLogs("=== _on_data_start = ")
				Global.saveFileLogs(on_data)
			for objekt in on_data['payload']['data']['objekto']['edges']:
				Global.direktebla_objekto[objekt['node']['realeco']['objId']-2]=objekt['node'].duplicate(true)
				Global.direktebla_objekto[objekt['node']['realeco']['objId']-2]['kosmo'] = Global.server
			# теперь загружаем те объекты, которые из представленных находятся в космосе
			var q = QueryObject.new()
			id_direktebla_kosmo_query = Net.current_query_id
			Net.current_query_id += 1
			Net.send_json(q.get_direktebla_kosmo_json(id_direktebla_kosmo_query))
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		# список управляемый объектов в космосе
		elif on_data['id'] == String(id_direktebla_kosmo_query):
			if on_data['payload'].get('data'):
				var uuid = []
				for pars in on_data['payload']['data']['filteredObjekto']['edges']:
					uuid.append(pars['node']['uuid'])
				if !Global.server:
					for objekt in Global.direktebla_objekto:
						if objekt.has('uuid'):
							if objekt['uuid'] in uuid:
								objekt['kosmo']=true
				Global.loading = true
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
			#отключаем перехват сигнала
			Net.disconnect("input_data", Callable(self, "_on_data_start"))
# warning-ignore:return_value_discarded
			Net.connect("input_data", Callable(self, "_on_data"))
			Global.objektoj.clear()
			load_objektoj_kosmo()
			#отображение и включение аудиоплеера
			if Global.status and Global.nickname:
				var gramofono = load("res://blokoj/gramofono/scenoj/gramofono.tscn")
				var gramofono_child = gramofono.instantiate()
				add_child(gramofono_child)
			$CanvasLayer/UI/UI/Taskoj.mendo_informoj_projekto()
			if (Global.server or Global.autoload) and Global.realeco!=1:
				go_realeco()
		i_data_server += 1
	for forigo in masivo_forigo:
		Net.data_server.remove(forigo)


# бинарный поиск
#var masivo = []# массив структур запроса к серверу, содержащий id
# в структуре обязано наличие:
# id - id самого запроса
# остальные поля структуры не важны
func BinaraSercxoId(masivo, val):
	var first = 0
	var last = len(masivo)-1
	var index = -1
	while (first <= last) and (index == -1):
		var mid = int((first+last)/2)
		if masivo[mid]['id'] == val:
			index = mid
		else:
			if val<masivo[mid]['id']:
				last = mid -1
			else:
				first = mid +1
	return index


# обработчик прихода постоянных данных
func _on_data():
	var i_data_server = 0
	var masivo_forigo = [] # массив индексов на удаление
	for on_data in Net.data_server:
		var index_stokejo = BinaraSercxoId(id_stokejo, int(on_data['id']))
		if !on_data['payload']['data']:
			print('=== error ===',on_data)
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		elif index_stokejo > -1: # находится в списке запрашиваемых данных по объекту
			var stokejo = on_data['payload']['data']['objektoStokejo']['edges'].front()['node']
			# пришли данные по складу
			# нужно обновление данных по объекту в списке, в космосе, в станции
			$CanvasLayer/UI/UI/Objektoj.aldoni_objektoj_stokejo(
				id_stokejo[index_stokejo]['uuid_objekto'],
				id_stokejo[index_stokejo]['uuid_modulo'],
				stokejo
			)
			# добавить проверку - если это управляемый корабль ???!!!
			# ('=== вызвали добавление данных в склад ')
			# если ещё нужно дополучить данных
			if stokejo['objekto']['pageInfo']['hasNextPage']:
				informmendo_stokejo(
					id_stokejo[index_stokejo]['uuid_objekto'],
					id_stokejo[index_stokejo]['uuid_modulo'],
					stokejo['uuid'],
					stokejo['objekto']['pageInfo']['endCursor']
				)
#			else:
				# ('=== закончили получение')
			id_stokejo.remove(index_stokejo) # удаляем из списка полученный ответ
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		elif on_data['id'] == String(id_ligilo):
			# вошли в станцию
			if Global.logs:
				print('===on_data === ', on_data)
				Global.saveFileLogs('========= on_data ==========')
				Global.saveFileLogs(on_data)
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']=\
				on_data['payload']['data']['objekto']['edges'].front()['node']['ligiloLigilo'].duplicate(true)
			id_ligilo = 0
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
			# включаем подписку на связь со станцией
			subscribtion_ligilo(on_data['payload']['data']['objekto']['edges'].front()['node']['ligiloLigilo']['edges'].front()['node']['uuid'])
			subscribtion_objekto_ligilo(on_data['payload']['data']['objekto']['edges'].front()['node']['ligiloLigilo']['edges'].front()['node']['posedanto']['uuid'])
			# запрос данных по станции
			load_stacio(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].front()['node']['posedanto']['uuid'],"")
		elif on_data['id'] == String(ligilo_subscription_id):
			# выходим из станции
			if on_data['payload']['data']['ligiloEventoj']['ligilo']['forigo']:
				# отписываемся
				nuligo_subscribtion_ligilo()
				nuligo_subscribtion_objekto_ligilo()
				var q = QueryObject.new()
				Net.net_id_clear.append(ligilo_subscription_id)
				Net.send_json(q.nuligo_subscribtion(ligilo_subscription_id))
				ligilo_subscription_id = 0
				# выходим из станции
				Global.direktebla_objekto[Global.realeco-2]['koordinatoX'] = \
					Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoX'] + 120
				Global.direktebla_objekto[Global.realeco-2]['koordinatoY'] = \
					Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoY'] + 120
				Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'] = \
					Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoZ'] + 200
				Global.kubo = Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['kubo']['objId']
				Global.direktebla_objekto[Global.realeco-2]['rotaciaX'] = 0
				Global.direktebla_objekto[Global.realeco-2]['rotaciaY'] = 0
				Global.direktebla_objekto[Global.realeco-2]['rotaciaZ'] = 0
				#удаляем в массиве объектов пользователя указатель на станцию
				Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].clear()
				Global.direktebla_objekto[Global.realeco-2]['kosmo'] = true
				CloseWindow()
				var err = get_tree().change_scene_to_file('res://blokoj/kosmo/scenoj/space.tscn')
				reloadWindow()
				if err:
					print('Ошибка загрузки сцены: ', err)
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		elif on_data['id'] == String(objekto_ligilo_subscription_id):
			# пришла информация об изменении связей внутри станции
			analizoObjektoLigiloSubscription(on_data['payload']['data'])
			# вызываем перезагрузку данных в складе при открытом складе
			if $CanvasLayer/UI/UI/items_menuo/VBox.visible:
				$CanvasLayer/UI/UI/items_menuo.plenigi_formularon()
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		elif on_data['id'] == String(subscription_uzanto_realeco_id):
			# пришла информация об изменении в реальности у пользователя
			if Global.fenestro_kosmo:
				Global.saveFileLogs('===== uzantoEventoj ======')
				Global.saveFileLogs(on_data['payload']['data']['uzantoEventoj']['objekto'])
				Global.fenestro_kosmo.sxangxi_shipo(on_data['payload']['data']['uzantoEventoj']['objekto'])
			elif Global.fenestro_stacio:
				pass
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		elif on_data['id'] == String(id_statio):
			id_statio = 0
			# пришли данные по станции
			if Global.fenestro_stacio:
				if on_data['payload']['data'].get('objekto'):
					Global.objektoj = []
					Global.fenestro_stacio.objekto = on_data['payload']['data']['objekto']['edges'].front()['node'].duplicate(true)
				# добавляем связи по объектам
				for ligilo in on_data['payload']['data']['objektoLigilo']['edges']:
					# проверяем на получение данных по управляемому кораблю и обновляем данные
					if ligilo['node']['ligilo']['uuid'] == Global.direktebla_objekto[Global.realeco-2]['uuid']:
						# обновляем управляемый объект
						# if Global.logs:
						# 	print('=== обновляем управляемый объект ===', ligilo['node']['ligilo']['uuid'])
						# 	Global.saveFileLogs('=== обновляем управляемый объект ===')
						# 	Global.saveFileLogs(Global.direktebla_objekto[Global.realeco-2])
#						Global.direktebla_objekto[Global.realeco-2] = null
#						Global.direktebla_objekto[Global.realeco-2] = ligilo['node']['ligilo'].duplicate(true)
						Global.direktebla_objekto[Global.realeco-2]['uuid'] = ligilo['node']['ligilo']['uuid']
						Global.direktebla_objekto[Global.realeco-2]['integreco'] = ligilo['node']['ligilo']['integreco']
						Global.direktebla_objekto[Global.realeco-2]['volumenoInterna'] = ligilo['node']['ligilo']['volumenoInterna']
						Global.direktebla_objekto[Global.realeco-2]['volumenoEkstera'] = ligilo['node']['ligilo']['volumenoEkstera']
						Global.direktebla_objekto[Global.realeco-2]['volumenoStokado'] = ligilo['node']['ligilo']['volumenoStokado']
						Global.direktebla_objekto[Global.realeco-2]['nomo']['enhavo'] = ligilo['node']['ligilo']['nomo']['enhavo']
						Global.direktebla_objekto[Global.realeco-2]['resurso']['objId'] = ligilo['node']['ligilo']['resurso']['objId']
						Global.direktebla_objekto[Global.realeco-2]['stato']['objId'] = ligilo['node']['ligilo']['stato']['objId']
						Global.direktebla_objekto[Global.realeco-2]['stato']['potenco'] = ligilo['node']['ligilo']['stato']['potenco']
						Global.direktebla_objekto[Global.realeco-2]['stato']['statoAntaua'] = ligilo['node']['ligilo']['stato']['statoAntaua']
						Global.direktebla_objekto[Global.realeco-2]['stato']['statoSekva']['integreco'] = ligilo['node']['ligilo']['stato']['statoSekva']['integreco']
						Global.direktebla_objekto[Global.realeco-2]['ligilo'] = null
						if ligilo['node']['ligilo'].get('ligilo'):
							Global.direktebla_objekto[Global.realeco-2]['ligilo'] = ligilo['node']['ligilo']['ligilo'].duplicate(true)
						Global.direktebla_objekto[Global.realeco-2]['kosmo'] = false
						$CanvasLayer/UI/UI/Objektoj.aldoni_objekto_ligilo(ligilo['node'],true)
					else:
						# по остальным кораблям записываем данные
						$CanvasLayer/UI/UI/Objektoj.aldoni_objekto_ligilo(ligilo['node'])
				if on_data['payload']['data']['objektoLigilo']['pageInfo']['hasNextPage']:
					load_stacio(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['uuid'],
						on_data['payload']['data']['objektoLigilo']['pageInfo']['endCursor'])
				else:
					# ('пришли все данные, обновляем список')
					if $CanvasLayer/UI/UI/konservejo/VBox.visible:
						$CanvasLayer/UI/UI/konservejo.plenigi_formularon()
			else:
				print('Станция не загрузилась!!!')
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		elif on_data['payload']['data'].get('objektoProjektoResursoStato'):
			# загрузка объектов
#			if Global.logs:
#				print(' пришла загрузка объектов ')
			for item in on_data['payload']['data']['objektoProjektoResursoStato']['edges']:
#				Global.saveFileLogs('item[node]')
#				Global.saveFileLogs(item['node'])
				aldoni_objekto(item['node'])
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
			if on_data['payload']['data']['objektoProjektoResursoStato']['pageInfo']['hasNextPage']:
				load_objektoj_kosmo(on_data['payload']['data']['objektoProjektoResursoStato']['pageInfo']['endCursor'])
			else:
				if $"/root".get_node_or_null('space'):# если загружен космос
					$"/root".get_node('space').emit_signal("load_objektoj")# загружаем объекты космоса
				emit_signal("load_objekto")
				if Global.logs:
					print('=== пришло последнее ============')
		i_data_server += 1
	for forigo in masivo_forigo:
		Net.data_server.remove(forigo)


# обновляем данные управляемого корабля
func aldoni_objekto_uzanto(objekto):
	# обновляем данные directebla по своему кораблю
	var kosmo = Global.direktebla_objekto[Global.realeco-2]['kosmo']
	Global.direktebla_objekto[Global.realeco-2]=objekto.duplicate(true)
	if Global.server:
		Global.direktebla_objekto[Global.realeco-2]['kosmo'] = true
	else:
		Global.direktebla_objekto[Global.realeco-2]['kosmo'] = kosmo
	# запрашиваем что хранится на складах по своему кораблю === !!!
	$CanvasLayer/UI/UI/Objektoj.aldoni_objekto(objekto,true)


# добавить объект в список или изменить главного
func aldoni_objekto(objekto):
	if objekto['uuid'] == Global.direktebla_objekto[Global.realeco-2]['uuid']:
		aldoni_objekto_uzanto(objekto)
	else:# свой корабль не добавляем в список
		$CanvasLayer/UI/UI/Objektoj.aldoni_objekto(objekto)


# удалить вложенный объект внутри списка управляемых объектов
func forigo_objekto_direkteble(uuid_objekto, uuid_ligilo):
	var i_ligilo = 0
	for ligilo in Global.direktebla_objekto[Global.realeco-2]['ligilo']['edges']:
		if ligilo['node']['uuid'] == uuid_ligilo:
			Global.direktebla_objekto[Global.realeco-2]['ligilo']['edges'].remove(i_ligilo)
			break
		var i_ligilo2 = 0
		for ligilo2 in ligilo['node']['ligilo']['ligilo']['edges']:
			if ligilo2['node']['uuid'] == uuid_ligilo:
				ligilo['node']['ligilo']['ligilo']['edges'].remove(i_ligilo2)
				break
			i_ligilo2 += 1
		i_ligilo += 1


func analizoObjektoLigiloSubscription(on_data):
	"""
	анализируем ответ по подписке в станции
	fadeno_ligilo - в какой ветке произошло изменение
	ligilo - в какой конкретно связи произошло изменение
	objekto_sxangxi - в каком объекте произошло изменение
	"""
	if Global.logs:
		print('== пришли данные по станции')
#		Global.saveFileLogs('== пришли данные по станции')
#		Global.saveFileLogs(on_data)
	if on_data['objektoLigiloEventoj']['ligilo']:
		# если произошло изменение в связи
		Title.get_node("CanvasLayer/UI/UI/Objektoj").sxangxi_ligilo(
			on_data['objektoLigiloEventoj']['fadenoLigilo'], 
			on_data['objektoLigiloEventoj']['ligilo'], 
			on_data['objektoLigiloEventoj']['objektoSxangxi']
		)
		if $CanvasLayer/UI/UI/konservejo/VBox.visible:
			$CanvasLayer/UI/UI/konservejo.plenigi_formularon()


func _on_Profilo_pressed():
	$CanvasLayer/UI/Popup_profilo.popup()
	$CanvasLayer/UI/Popup_profilo/profilo1/VBox.set_visible(true)

func _on_Objektoj_pressed():
	if $CanvasLayer/UI/UI/Objektoj/VBox.visible:
		$CanvasLayer/UI/UI/Objektoj/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/UI/Objektoj/VBox.size =  Vector2(250, 700)
		$CanvasLayer/UI/UI/Objektoj/VBox.set_visible(true)
		$CanvasLayer/UI/UI/Objektoj.fenestro_supren()


#func set_visible(visible: bool):
#	$CanvasLayer/UI.visible = visible
	

func _on_Taskoj_pressed():
	$CanvasLayer/UI/UI/Taskoj/VBox.set_visible(true)
	$CanvasLayer/UI/UI/Taskoj.fenestro_supren()


# закрываем все окна
func CloseWindow():
	$CanvasLayer/UI/UI/Taskoj.clear_all()
	$CanvasLayer/UI/UI/Taskoj/VBox.set_visible(false)
	$CanvasLayer/UI/UI/Objektoj/VBox.set_visible(false)
	$CanvasLayer/UI/UI/itinero/VBox.set_visible(false)
	$CanvasLayer/UI/UI/interago/VBox.set_visible(false)
	$CanvasLayer/UI/UI/komerco/VBox.set_visible(false)

func reloadWindow():
	$CanvasLayer/UI/UI/Taskoj.mendo_informoj_projekto()
	Global.objektoj.clear()
	if Global.realeco>1:
		# если в космосе
		if Global.direktebla_objekto[Global.realeco-2]['kosmo'] or Global.server:
			load_objektoj_kosmo()
		else:
			if Global.direktebla_objekto[Global.realeco-2].get('ligiloLigilo'):
				load_stacio(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['uuid'],"")

				
# запрашиваем данные по связи с объектом
func objekto_link(uuid_objekto):
	# нужно получить в ответ uuid связи
	id_ligilo = Net.get_current_query_id()
	# Делаем запрос к бэкэнду
	var q = QueryObject.new()
	Net.send_json(q.get_objekto_uuid(
			uuid_objekto, 
			id_ligilo))


func on_cap():
	"""
	переход в триумфо
	"""
	var mezo_cap = preload("res://kerno/menuo/resursoj/icons/tab4_3.png")
	$CanvasLayer/UI/mezo_regions.icon = mezo_cap
	$CanvasLayer/UI/real/real_cap_rect.set_visible(true)
	$CanvasLayer/UI/real/real_com_rect.set_visible(false)
	$CanvasLayer/UI/com/com_cap_rect.set_visible(true)
	$CanvasLayer/UI/com/com_real_rect.set_visible(false)
	$CanvasLayer/UI/cap/cap_com_rect.set_visible(false)
	$CanvasLayer/UI/cap/cap_real_rect.set_visible(false)
	$CanvasLayer/UI/Lbar.color = Color(0, 0.407843, 0.407843, 0.862745)
	$CanvasLayer/UI/Lbar3.self_modulate = Color(0, 0.407843, 0.407843, 0.862745)
	$CanvasLayer/UI/real/realLabel.set_visible(false)
	$CanvasLayer/UI/com/comLabel.set_visible(false)
	$CanvasLayer/UI/cap/capLabel.set_visible(true)
	$CanvasLayer/UI/romb.color = Color(0, 0.407843, 0.407843, 0.862745)
	startigi_sceno()
	subscribtion_uzanto_realeco()


func _on_cap_pressed():
	"""
	нажата кнопка перехода в триумфо
	"""
	if Global.loading: #здесь вызывать задержку надо
		var reload = false
		if Global.realeco!=3:
			nuligo_subscribtion_uzanto_realeco()
			Global.realeco = 3
			reload = true
			# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
			CloseWindow()
			on_cap()
		if reload:
			reloadWindow()
	else:
		print('Ещё не загружено')


# запусти́ть III  1. (включить: мотор, станок, компьютер) startigi, ekfunkciigi;
# сце́на sceno
func startigi_sceno():
	"""
	запустить сцену параллельного мира
	"""
	# если объекта не будет в космосе, то загружать станцию
	if Global.direktebla_objekto[Global.realeco-2]['kosmo'] or Global.server:
# warning-ignore:return_value_discarded
		get_tree().change_scene_to_file('res://blokoj/kosmo/scenoj/space.tscn')
	else:
		if Global.direktebla_objekto[Global.realeco-2].get('ligiloLigilo'):
			subscribtion_ligilo(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].front()['node']['uuid'])
			subscribtion_objekto_ligilo(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].front()['node']['posedanto']['uuid'])
		# при разных станциях доработать выбор сцены станции
# warning-ignore:return_value_discarded
		get_tree().change_scene_to_file('res://blokoj/kosmostacioj/CapKosmostacio.tscn')
#		get_tree().change_scene("res://blokoj/kosmostacioj/ComKosmostacio.tscn")


func on_com():
	var mezo_com = preload("res://kerno/menuo/resursoj/icons/tab4_1.png")
	$CanvasLayer/UI/mezo_regions.icon = mezo_com
	$CanvasLayer/UI/real/real_cap_rect.set_visible(false)
	$CanvasLayer/UI/real/real_com_rect.set_visible(true)
	$CanvasLayer/UI/com/com_cap_rect.set_visible(false)
	$CanvasLayer/UI/com/com_real_rect.set_visible(false)
	$CanvasLayer/UI/cap/cap_com_rect.set_visible(true)
	$CanvasLayer/UI/cap/cap_real_rect.set_visible(false)
	$CanvasLayer/UI/Lbar.color = Color(0.333333, 0, 0.066667, 0.862745)
	$CanvasLayer/UI/Lbar3.self_modulate = Color(0.333333, 0, 0.066667, 0.862745)
	$CanvasLayer/UI/real/realLabel.set_visible(false)
	$CanvasLayer/UI/com/comLabel.set_visible(true)
	$CanvasLayer/UI/cap/capLabel.set_visible(false)
	$CanvasLayer/UI/romb.color = Color(0.333333, 0, 0.066667, 0.862745)
	startigi_sceno()
	subscribtion_uzanto_realeco()


func _on_com_pressed():
	if Global.loading: #здесь вызывать задержку надо
		var reload = false
		if Global.realeco!=2:
			nuligo_subscribtion_uzanto_realeco()
			Global.realeco = 2
			reload = true
			# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
			CloseWindow()
			on_com()
		if reload:
			reloadWindow()
	else:
		print('Ещё не загружено')


func on_real():
	var mezo_real = preload("res://kerno/menuo/resursoj/icons/tab4_2.png")
	$CanvasLayer/UI/mezo_regions.icon = mezo_real
	$CanvasLayer/UI/real/real_cap_rect.set_visible(false)
	$CanvasLayer/UI/real/real_com_rect.set_visible(false)
	$CanvasLayer/UI/com/com_cap_rect.set_visible(false)
	$CanvasLayer/UI/com/com_real_rect.set_visible(true)
	$CanvasLayer/UI/cap/cap_com_rect.set_visible(false)
	$CanvasLayer/UI/cap/cap_real_rect.set_visible(true)
	$CanvasLayer/UI/Lbar.color = Color(0.4, 0.6, 1, 0.862745)
	$CanvasLayer/UI/Lbar3.self_modulate = Color(0.4, 0.6, 1, 0.862745)
	$CanvasLayer/UI/real/realLabel.set_visible(true)
	$CanvasLayer/UI/com/comLabel.set_visible(false)
	$CanvasLayer/UI/cap/capLabel.set_visible(false)
	$CanvasLayer/UI/romb.color = Color(0.4, 0.6, 1, 0.862745)
	var err = get_tree().change_scene_to_file("res://blokoj/kosmostacioj/Kosmostacio.tscn")
	if err:
		print('ошибка смены сцены = ',err)
	subscribtion_uzanto_realeco()


func _on_real_pressed():
	var reload = false
	if Global.realeco!=1:
		nuligo_subscribtion_uzanto_realeco()
		Global.realeco = 1
		reload = true
		# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
		CloseWindow()
		on_real()
# warning-ignore:return_value_discarded
	if reload:
		reloadWindow()


func _on_b_itinero_pressed():
	if $CanvasLayer/UI/UI/itinero/VBox.visible:
		$CanvasLayer/UI/UI/itinero/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/UI/itinero/VBox.set_visible(true)
		$CanvasLayer/UI/UI/itinero.fenestro_supren()


func _on_ad_pressed():
	$CanvasLayer/UI/UI/ad_1/CanvasLayer/Margin.set_visible(true)
	$CanvasLayer/UI/UI/ad_1.fenestro_supren()


func _on_interago_pressed():
	if $CanvasLayer/UI/UI/interago/VBox.visible:
		$CanvasLayer/UI/UI/interago/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/UI/interago.print_button()
		$CanvasLayer/UI/UI/interago/VBox.set_visible(true)
		$CanvasLayer/UI/UI/interago.fenestro_supren()


func _on_eliro_button_up():
	$CanvasLayer/UI/UI/eliro.popup_centered()


func _input(event):
	if event is InputEvent and Input.is_key_pressed(KEY_ESCAPE):
		$CanvasLayer/UI/UI/eliro.popup_centered()


# запрашиваем объекты, которые в космосе
func load_objektoj_kosmo(after=""):
	var q = QueryObject.new()
	id_objekto_kosmo = Net.get_current_query_id()
#	зугрузка объектов партиями, количество в партии указано в константе запроса
	Net.send_json(q.get_objekto_kosmo(Net.statuso_laboranta, 
		Net.tasko_tipo_objekto, Global.kubo, id_objekto_kosmo, after))


# запрашиваем данные по станции
func load_stacio(uuid_statio, after):
	var q = QueryObject.new()
	id_statio = Net.get_current_query_id()
	Net.send_json(q.get_objekto(uuid_statio,id_statio,after))


# запрашиваем данные по объекту
# informmendo - запросить (све́дений) (тж. инф.)
func informmendo_objekto(uuid_objekto, after):
	var q = QueryObject.new()
	var tempo =  Net.get_current_query_id()
	id_objekto.append({
		'uuid' : uuid_objekto,
		'id' : tempo
	})
	Net.send_json(q.get_objekto(uuid_objekto,tempo,after))


# запрашиваем данные по складу
# informmendo - запросить (све́дений) (тж. инф.)
func informmendo_stokejo(uuid_objekto, uuid_modulo, uuid_stokejo, after):
	var q = QueryObject.new()
	var tempo =  Net.get_current_query_id()
	id_stokejo.append({
		'uuid_objekto': uuid_objekto,
		'uuid_modulo': uuid_modulo,
		'id': tempo
	})
	Net.send_json(q.get_stokejo(uuid_stokejo,tempo,after))


func _on_komerco_button_up():
	if $CanvasLayer/UI/UI/komerco/VBox.visible:
		$CanvasLayer/UI/UI/komerco/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/UI/komerco/VBox.set_visible(true)
		$CanvasLayer/UI/UI/komerco.mendi_informoj()


func _on_konservejo_pressed():
	if $CanvasLayer/UI/UI/konservejo/VBox.visible:
		$CanvasLayer/UI/UI/konservejo/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/UI/konservejo/VBox.set_visible(true)
		$CanvasLayer/UI/UI/konservejo.fenestro_supren()




func _on_indicoj_pressed():
	if $CanvasLayer/UI/UI/indicoj/VBox.visible:
		$CanvasLayer/UI/UI/indicoj/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/UI/indicoj/VBox.set_visible(true)
		$CanvasLayer/UI/UI/indicoj.fenestro_supren()


func _on_items_pressed():
	if $CanvasLayer/UI/UI/proprietajxo/VBox.visible:
		$CanvasLayer/UI/UI/proprietajxo/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/UI/proprietajxo/VBox.set_visible(true)
		$CanvasLayer/UI/UI/proprietajxo.mendi_informoj()


# подписка на связь со станцией для выхода из станции версии 1
func subscribtion_ligilo(uuid_ligilo):
	var q = QueryObject.new()
	ligilo_subscription_id = Net.get_current_query_id()
	if Global.logs:
		print('==установлена подписка для выхода из станции = ',ligilo_subscription_id)
	Net.send_json(q.subscription_ligilo(uuid_ligilo, ligilo_subscription_id))


# подписка на связь со станцией
func subscribtion_objekto_ligilo(uuid_objekto):
	var q = QueryObject.new()
	objekto_ligilo_subscription_id = Net.get_current_query_id()
	if Global.logs:
		print('==установлена подписка для объкта-станции = ',objekto_ligilo_subscription_id)
	Net.send_json(q.subscription_objekto_ligilo(uuid_objekto, objekto_ligilo_subscription_id))


# отписка от связи со станцией для выхода из станции версии 1
# nuligo - отмена (эсперанто)
func nuligo_subscribtion_ligilo():
	var query = JSON.stringify({
		'type': 'stop',
		'id': '%s' % ligilo_subscription_id})
	Net.send_json(query)
	ligilo_subscription_id = 0


# отписка от связи со станцией
# nuligo - отмена (эсперанто)
func nuligo_subscribtion_objekto_ligilo():
	var query = JSON.stringify({
		'type': 'stop',
		'id': '%s' % objekto_ligilo_subscription_id})
	Net.send_json(query)
	objekto_ligilo_subscription_id = 0


func _on_tempo_informpeto_pressed():
	if $CanvasLayer/UI/UI/tempo_informpeto/VBox.visible:
		$CanvasLayer/UI/UI/tempo_informpeto/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/UI/tempo_informpeto/VBox.set_visible(true)
		$CanvasLayer/UI/UI/tempo_informpeto.fenestro_supren()


func _on_Timer_timeout():
	if (not Global.logs) or (not Net.flago_tempo_informpeto):
		return
	var path = "res://tempo_informpeto.log"
	#var file = File.new()
	#var err = file.open(path, File.WRITE)
	var file = FileAccess.open
	var err = file.open(path, FileAccess.WRITE)
	if err != OK:
		print('ошибка создания файла логов среднего времени')
	file.seek_end()
#	file.store_string(str(Net.tempo_informpeto))
	for informpeto in Net.tempo_informpeto:
		var value = Net.tempo_informpeto.get(informpeto)
		file.store_string('\n' + informpeto + \
			'   количество:' + str(value['kvanto']) +\
			'   среднее время:' + str(value['averagxo']))
	file.close() 
	Net.flago_tempo_informpeto = false


func _on_administri_pressed():
	if $CanvasLayer/UI/UI/main_administri/VBox.visible:
		$CanvasLayer/UI/UI/main_administri/VBox.set_visible(false)
	else:
		if len($CanvasLayer/UI/UI/main_administri.lingvoj) == 0:
			#запрашиваем языки
			$CanvasLayer/UI/UI/main_administri.queryLingvo()
		$CanvasLayer/UI/UI/main_administri/VBox.set_visible(true)
		$CanvasLayer/UI/UI/main_administri/VBox.global_position = Vector2(48, 48)
		$CanvasLayer/UI/UI/main_administri.plenigi_formularon()
		$CanvasLayer/UI/UI/main_administri.fenestro_supren()


# подписка на действие в реальности нахождения
func subscribtion_uzanto_realeco():
	var q = QueryObject.new()
	subscription_uzanto_realeco_id = Net.get_current_query_id()
	if Global.logs:
		print('===subscription_uzanto_realeco_id === ',subscription_uzanto_realeco_id)
	Net.send_json(q.subscribtion_uzanto_realeco(subscription_uzanto_realeco_id))


# отписка от действий в кубе
# nuligo - отмена (эсперанто)
func nuligo_subscribtion_uzanto_realeco():
	if not subscription_uzanto_realeco_id:
		return
	var query = JSON.stringify({
		'type': 'stop',
		'id': '%s' % subscription_uzanto_realeco_id})
	if Global.logs:
		print('=== nuligo_subscribtion_uzanto_realeco =query= ',query)
	Net.send_json(query)
	subscription_uzanto_realeco_id = null


