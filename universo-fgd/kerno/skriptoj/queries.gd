extends Node
# Здесь будем хранить глобальные (используемые в нескольких блоках) запросы к бэкэнду


# Изменение статуса задачи, по умолчанию - завершение задачи (или другой статус)
func finado_tasko(tasko_uuid, statusoId = Net.statuso_fermo, id=0):
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.push_back(id)
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($uuid:UUID,  '+
		' $statusoId:Int, )'+
		'{ redaktuTaskojTasko (uuid: $uuid,  '+
		' statusoId:$statusoId) { status '+
		' message taskoj { uuid } } }',
		'variables': {"uuid":tasko_uuid, "statusoId": statusoId } }})
#	if Global.logs:
#		print('===finado_tasko==',query)
	Net.add_net_sendij(id, 'finado_tasko')
	return query


# завершение проекта
func finado_projekto(projekto_uuid, id=0):
	var statusoId = 4 # Закрыт 
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.push_back(id)
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($projekto_uuid:UUID, '+
		' $statusoId:Int, )'+
		'{ redaktuTaskojProjekto (uuid: $projekto_uuid,  '+
		' statusoId:$statusoId) { status '+
		' message projekto { uuid } } '+
		'}',
		'variables': {"statusoId": statusoId, "projekto_uuid":projekto_uuid } }})
	Net.add_net_sendij(id, 'finado_projekto')
	return query


# завершение задачи и проекта
func finado_projekto_tasko(projekto_uuid, tasko_uuid, id=0):
	var statusoId = 4
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.push_back(id)
	Net.add_net_sendij(id, 'finado_projekto_tasko')
	return JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($tasko_uuid:UUID, $projekto_uuid:UUID, '+
		' $statusoId:Int, ) {'+
		'redaktuTaskojProjekto (uuid: $projekto_uuid,  '+
		' statusoId:$statusoId) { status '+
		' message projekto { uuid } } '+
		'redaktuTaskojTasko (uuid: $tasko_uuid,  '+
		' statusoId:$statusoId) { status '+
		' message taskoj { uuid } } '+
		'}',
		'variables': {"tasko_uuid":tasko_uuid, "statusoId": statusoId, "projekto_uuid":projekto_uuid } } })


