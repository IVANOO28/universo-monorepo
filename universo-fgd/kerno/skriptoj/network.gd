# websocket_client.gd
# This file is part of: NodeWebSockets
# Copyright (c) 2023 IcterusGames
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the 
# "Software"), to deal in the Software without restriction, including 
# without limitation the rights to use, copy, modify, merge, publish, 
# distribute, sublicense, and/or sell copies of the Software, and to 
# permit persons to whom the Software is furnished to do so, subject to 
# the following conditions: 
#
# The above copyright notice and this permission notice shall be 
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
@icon("res://addons/nodewebsockets/nws_client.svg")
class_name WebSocketClient
extends Node
## A WebSocket client node implementation.
##
## [b]Usage:[/b][br]
## Simply add [WebSocketClient] to your scene, config the server on the 
## inspector and connect the events that you need.[br]


## Emitted when the connection to the server is closed. [param was_clean_close] 
## will be true if the connection was shutdown cleanly.
signal connection_closed(was_clean_close : bool)

## Emitted when the connection to the server fails.
signal connection_error(error : Error)

## Emitted when a connection with the server is established, [param protocol] 
## will contain the sub-protocol agreed with the server.
signal connection_established(peer : WebSocketPeer, protocol : String)

## Emitted when a message is received.
signal data_received(peer : WebSocketPeer, message, is_string : bool)

## Emitted when a message text is received.
signal text_received(peer : WebSocketPeer, message)

## Emitted when the server requests a clean close. You should keep polling 
## until you get a [signal connection_closed] signal to achieve the clean 
## close. See [method WebSocketPeer.close] for more details.
signal server_close_request(code : int, reason : String)


enum POLL_MODE {
	MANUAL,  ## You must to call [method poll] regulary to process any request
	IDLE,    ## poll is called automaticaly on [method Node._process]
	PHYSICS, ## poll is called automaticaly on [method Node._physics_process]
}


## If true the client start listening when [method Node._ready] is called, if 
## false you will need to start the client calling [method connect_to_server]
@export var start_on_ready := true
## Setup the way to call [method poll]
@export var poll_mode : POLL_MODE = POLL_MODE.IDLE
## URL to WebSockets server[br][br]
## [b]Note:[/b] For TLS connections remember use a "wss://" prefix
@export var url_server : String = "ws://localhost:8000/api/v1.1/ws/"
## The server sub-protocols allowed during the WebSocket handshake.
@export var protocols := PackedStringArray()
@export_group("Client parameters")
## The extra HTTP headers to be sent during the WebSocket handshake.
## [b]Note:[/b] Not supported in Web exports due to browsers' restrictions.
@export var extra_headers := PackedStringArray()
## Creates an unsafe TLS client configuration where certificate validation is 
## optional. You can optionally provide a valid trusted_chain, but the common 
## name of the certificates will never be checked.[br][br]
## [b]Using this configuration for purposes other than testing is not 
## recommended.[/b]
@export var trusted_unsafe : bool = false
## see [method TLSOptions.client]
@export var trusted_chain : X509Certificate = null
## see [method TLSOptions.client]
@export var trusted_common_name_override : String = ""
## The size of the input buffer in bytes (roughly the maximum amount of memory that will be allocated for the inbound packets).
@export_range(1, 0x7FFFFFFF) var inbound_buffer_size : int = 65536
## The maximum amount of packets that will be allowed in the queues (both inbound and outbound).
@export_range(1, 0xFFFF) var max_queued_packets : int = 2048
## The size of the input buffer in bytes (roughly the maximum amount of memory that will be allocated for the outbound packets).
@export_range(1, 0x7FFFFFFF) var outbound_buffer_size : int = 65536

var _client := WebSocketPeer.new()
var _is_connected := false


func _ready():
	set_process(false)
	set_physics_process(false)
	if start_on_ready:
		connect_to_server.call_deferred()


func _process(_delta):
	if poll_mode != POLL_MODE.IDLE:
		set_process(false)
		return
	poll()


func _physics_process(_delta):
	if poll_mode != POLL_MODE.PHYSICS:
		set_physics_process(false)
		return
	poll()


## Connect to the server given by [member url_server]
func connect_to_server() -> Error:
	return connect_to_url(url_server)


## Connects to the given URL requesting one of the given 
## [param array_protocols] as sub-protocol. If the list empty (default), no 
## sub-protocol will be requested.[br]
## [br]
## You can optionally pass a list of [param array_headers] to be added to the 
## handshake HTTP request.
func connect_to_url(url : String, array_protocols = ["graphql-ws"], array_headers = null) -> Error:
	if _client.get_ready_state() != WebSocketPeer.STATE_CLOSED:
		connection_error.emit(ERR_ALREADY_IN_USE)
		return ERR_ALREADY_IN_USE
	url_server = url
	_is_connected = false
	if array_protocols != null:
		protocols = array_protocols
	if array_headers != null:
		extra_headers = array_headers
	_client.supported_protocols = protocols
	_client.handshake_headers = extra_headers
	_client.inbound_buffer_size = inbound_buffer_size
	_client.max_queued_packets = max_queued_packets
	_client.outbound_buffer_size = outbound_buffer_size
	var result : Error = OK
	var tls_options = null
	if trusted_unsafe:
		tls_options = TLSOptions.client_unsafe(trusted_chain)
	elif trusted_chain != null or trusted_common_name_override.length() > 0:
		tls_options = TLSOptions.client(trusted_chain, trusted_common_name_override)
	result = _client.connect_to_url(url, tls_options)
	if result == OK:
		match poll_mode:
			POLL_MODE.MANUAL:
				set_process(false)
				set_physics_process(false)
			POLL_MODE.IDLE:
				set_process(true)
				set_physics_process(false)
			POLL_MODE.PHYSICS:
				set_process(false)
				set_physics_process(true)
	else:
		connection_error.emit(result)
	return result


## Return true if is connected to server
func is_listening() -> bool:
	return _client.get_ready_state() != WebSocketPeer.STATE_CLOSED


## Disconnects this client from the connected host.[br]
## See [method WebSocketPeer.close] for more information.
func disconnect_from_host(code : int = 1000, reason : String = "") -> void:
	_client.close(code, reason)


## Disconnects this client from the connected host.[br]
## See [method WebSocketPeer.close] for more information.
func close(code : int = 1000, reason : String = "") -> void:
	_client.close(code, reason)


## Return the IP address of the currently connected host.
func get_connected_host() -> String:
	return _client.get_connected_host()


## Return the IP port of the currently connected host.
func get_connected_port() -> int:
	return _client.get_connected_port()


## Return the [class WebSocketPeer] of this client
func get_peer() -> WebSocketPeer:
	return _client


func poll() -> void:
	_client.poll()
	match _client.get_ready_state():
		WebSocketPeer.STATE_CONNECTING:
			print_verbose("WebSocketClient: connecting to \"", url_server, "\" ...")
		
		WebSocketPeer.STATE_OPEN:
			if not _is_connected:
				print_verbose("WebSocketClient: connection established.")
				_is_connected = true
				connection_established.emit(_client, _client.get_selected_protocol())
			while _client.get_available_packet_count():
				var message = _client.get_packet()
				var is_string = _client.was_string_packet()
				var error = _client.get_packet_error()
				if error != OK:
					print_verbose("WebSocketClient: packet recived with error: ", error, " is string: ", is_string, " packet: ", message)
					continue
				if is_string:
					var msg_str = message.get_string_from_utf8()
					data_received.emit(_client, message, is_string)
					text_received.emit(_client, msg_str)
				else:
					data_received.emit(_client, message, is_string)
		
		WebSocketPeer.STATE_CLOSING:
			print_verbose("State closing...")
			# Keep polling to achieve proper close.
		
		WebSocketPeer.STATE_CLOSED:
			var code = _client.get_close_code()
			var reason = _client.get_close_reason()
			print_verbose("WebSocketClient: closed with code: %d, reason %s. Clean: %s" % [code, reason, code != -1])
			server_close_request.emit(code, reason)
			connection_closed.emit(code != -1)
			set_process(false)
			set_physics_process(false)


## Configures the buffer sizes for this [WebSocketPeer].[br]
## [br]
## The first two parameters define the size and queued packets limits of the 
## input buffer, the last are for the output buffer.[br]
## [br]
## Buffer sizes are expressed in KiB
func set_buffers(_input_buffer_size_kb: int, _input_max_packets: int, _output_buffer_size_kb: int) -> Error:
	if _client.get_ready_state() == WebSocketPeer.STATE_OPEN:
		return ERR_ALREADY_IN_USE
	if _input_buffer_size_kb <= 0 or _input_max_packets <= 0 or _output_buffer_size_kb <= 0:
		return ERR_PARAMETER_RANGE_ERROR
	inbound_buffer_size = _input_buffer_size_kb * 1024
	max_queued_packets = _input_max_packets
	outbound_buffer_size = _output_buffer_size_kb * 1024
	return OK


## see [method WebSocketPeer.send]
func send(message : PackedByteArray, write_mode : WebSocketPeer.WriteMode = 1) -> Error:
	return _client.send(message, write_mode)


## see [method WebSocketPeer.send_text]
func send_text(message : String) -> Error:
	return _client.send_text(message)


# extends Node

# # WebSocket - клиент
# class_name GraphQLWSConnector


# Signal to let GUI know whats up
signal connection_failed()
signal connection_succeeded()
signal server_disconnected()
signal input_data()


signal tempo_informpetoj() # изменилось среднее время вызова


# # настройки по справочнику
# # константы соответствия категорий, статусов и т.п.
# # категории задач:
# const kategorio_movado = 3 # категория движения объекта
# const kategorio_eniri_kosmostacio = 8 # категория входа в станцию
# const kategorio_eliro_kosmostacio = 9 # категория выхода из станции
# const tasko_kategorio_celilo = 6 # категория задачи прицеливания
# const tasko_kategorio_pafo = 7 # категория задачи выстрела
# const projekto_kategorio_pafado = 6 # категория проекта стрельбы (включает задачи прицеливания и выстрела)
# const projekto_tipo_objekto = 2 # тип проектов "Для объектов"
# const tasko_tipo_objekto = 2 # тип задач "Для объектов"
# const statuso_nova = 1 # статус "Новый"
# const statuso_laboranta = 2 # статус "В работе"
# const statuso_fermo = 4 # статус "Закрыт"
# const status_pauzo = 6 # статус "Приостановлен"
# const statuso_posedanto = 1 # статус владельца "Активный"
# const tipo_posedanto = 1 # тип владения "Полное владение"


# пришедшие данные с сервера
var data_server = []


# # запросы отправляются с очередным id 
# var current_query_id = 1

var net_id_clear = [] # список пустых id запросов (ответы не анализируются, а удаляются)
var net_sendij = [] # список отправленных запросов с их id, именем запроса, временем отправки
# # id, nomo_informpeto, tempo_sendij

# # к какому серверу подключаться
# var url_server = null
var http = "https"
var ws = "wss"
# @export var websocket_url = "" 
# #var _client := WebSocketClient.new()
# var _client := WebSocketPeer.new()
var connected = false


# # Хэш со всеми игроками зарегистрированными на сервере {id: name}
# # заполняется функцией register_player()
# var players = {}


# # словарь среднего времени запросов к серверу
var tempo_informpeto = {} #время_запрос информации
var flago_tempo_informpeto = false # флаг, что производилась запись. Для регулярной записи в файл. Запись в файл сбрасывает данный флаг


# func _ready():
# 	if not url_server:
# 		url_server = "t34.universo.pro"
# 		# url_server = "t54.tehnokom.su"
# 		# url_server = "t34.tehnokom.su"
# 		# url_server = "t34-85.tehnokom.su"
# 	websocket_url = ws+"://"+url_server+"/api/v1.1/ws/"
# 	#_client.connect("connection_closed", Callable(self, "_closed"))
# 	#_client.connect("connection_error", Callable(self, "_error"))
# 	#_client.connect("connection_established", Callable(self, "_connected"))
# 	#_client.connect("data_received", Callable(self, "_on_data"))


# func _closed(was_clean = false):
# 	# was_clean will tell you if the disconnection was correctly notified
# 	# by the remote peer before closing the socket.
# 	print("Closed, clean: ", was_clean)
# 	connected = false
# 	# set_process(false)
# 	emit_signal("server_disconnected")
# 	connect_to_server()


# func _error(was_clean = false):
# 	print("Closed, error: ", was_clean)
# 	connected = false
# 	emit_signal("connection_failed")
# 	connect_to_server()


# func _connected(proto = ""):
# 	print("Connected with protocol: ", proto)
# 	# Вот тут отправляем инициирущий пакет
# 	_client.get_peer(1).put_packet(JSON.stringify({
# 			'type': 'connection_init',
# 			'payload': {}
# 	}).to_utf8_buffer())


# получение данных с сервера
func _on_data():
	# Print the received packet, you MUST always use get_peer(1).get_packet
	# to receive data from server, and not get_packet directly when not
	# using the MultiplayerAPI.
	var jdata = _client.get_peer(1).get_packet().get_string_from_utf8()
	var test_json_conv = JSON.new()
	# TODO: Refactor 
	# test_json_conv.parse(jdata).result
	var data = test_json_conv.get_data()

	if data.has('type') && data['type'] == 'connection_ack':
		connected = true
		emit_signal("connection_succeeded")
	if data.has('type') && data['type'] == 'data':
		# проверяем, нужно ли обрабатывать данный id
		var index_net = net_id_clear.find(int(data['id']))
		var sendi = sercxo_net_sendij(int(data['id']))
		if sendi > -1:
			# если есть в списке отправленных запросов для статистики
			add_tempo_informpeto(net_sendij[sendi]['nomo_informpeto'], 
				Time.get_ticks_msec() - net_sendij[sendi]['tempo'])
			net_sendij.remove(sendi)
		if index_net > -1: # находится в списке очищаемых запросов
			net_id_clear.remove(index_net)
		else:
			data_server.append(data)
			emit_signal("input_data")


# func _process(_delta):
# 	# Что бы получать данные с сервера
# 	_client.poll()


# func _exit_tree():
# 	_client.disconnect_from_host()


# # отправка данных на сервер
# func send_data(data, id=0): # если id==0 то используем current_query_id
# 	if !id:
# 		id = current_query_id
# 		current_query_id += 1
# 		add_net_sendij(id,'send_data')
# 	var query = JSON.stringify({
# 		'type': 'start',
# 		'id': '%s' % id,
# 		'payload': data
# 	})
# 	_client.get_peer(1).put_packet(query.to_utf8_buffer())
# 	return id


# # отправка данных на сервер
# # nomo_informpeto - имя запроса для сбора статистики
# func send_json(data):
# 	_client.get_peer(1).put_packet(data.to_utf8_buffer())


# func connect_to_server():
# 	# print('===input WS')
# 	var protocols2 = PackedStringArray(["graphql-ws"])
# #	var err = _client.connect_to_url(websocket_url, protocols2, false, Global.backend_headers)
# 	var err = _client.connect_to_url(websocket_url, protocols2)
# 	if err != OK:
# 		print("Unable to connect = ", err)
# 	else:
# #		_client.get_peer(1).set_write_mode(WebSocketPeer.WRITE_MODE_TEXT)
# 		_client.get_peer(1).set_write_mode(WebSocketPeer.WRITE_MODE_TEXT)
		
# 	return err


# # передаёт корректный очередной номер и увеличивает его для следующей передачи
# func get_current_query_id():
# 	var id = current_query_id
# 	current_query_id += 1
# 	return id


# func get_current_query_id_and_liberigi():
# 	"""
# 	передаёт корректный очередной номер, увеличивает его для следующей передачи и ответ ставит в очередь на удаление из списка полученных ответов
# 	liberigi - очистить  (от чего-л. занимающего место; от чего-л. мешающего) 
# 	"""
# 	var id = get_current_query_id()
# 	net_id_clear.append(id)
# 	return id 


func add_net_sendij(id, nomo_informpeto):
	net_sendij.append({'id': id,
		'nomo_informpeto': nomo_informpeto,
		'tempo': Time.get_ticks_msec()})


 # поиск по id в списке отправленных запросов
 # возвращает порядковый номер в массиве, если нет, то возвращает -1
func sercxo_net_sendij(id):
	var index = -1
	var pasxo = 0 # шаг
	for sendi in net_sendij:
		if id == sendi['id']:
			index = pasxo
			return index
		pasxo += 1
	return -1

# добавляем в словарь отправленное время запроса
# nomo_informpeto - имя запроса
# tempo - прошедшее время запроса
func add_tempo_informpeto(nomo_informpeto, tempo):
	var nomo = tempo_informpeto.get(nomo_informpeto)
	if nomo:
		var kvanto = nomo['kvanto'] + 1
		var tempoj = nomo['tempo'] + tempo
		tempo_informpeto[nomo_informpeto] = {
			'kvanto': kvanto,
			'tempo': tempoj,
			'averagxo': tempoj / kvanto #averaĝo - среднее
		}
	else:
		tempo_informpeto[nomo_informpeto] = {
			'kvanto': 1,
			'tempo': tempo,
			'averagxo': tempo
		}
	flago_tempo_informpeto = true
	emit_signal("tempo_informpetoj")
