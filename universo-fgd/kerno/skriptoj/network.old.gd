extends Node

# Game port and ip
const ip = "159.69.44.27"
#const ip = "127.0.0.1"
const DEFAULT_PORT = 44444

# Signal to let GUI know whats up
signal connection_failed()
signal connection_succeeded()
signal server_disconnected()
signal players_updated()
signal message_received()

# Имя этого игрока. Пока берём из Global.login (см. _ready())
var my_name = ""
# ID этого игрока. Устанавливается функцией register_player()
# TODO: удалить, если не будем использовать
var my_id

# Хэш со всеми игроками зарегистрированными на сервере {id: name}
# заполняется функцией register_player()
var players = {}


func _ready():
	my_name = Global.login
	get_tree().connect("connected_to_server", Callable(self, "_connected_ok"))
	get_tree().connect("connection_failed", Callable(self, "_connected_fail"))
	get_tree().connect("server_disconnected", Callable(self, "_server_disconnected"))


func connect_to_server():
	var host = ENetMultiplayerPeer.new()
	host.create_client(ip, DEFAULT_PORT)
	get_tree().set_multiplayer_peer(host)


# Callback from SceneTree, called when connect to server
func _connected_ok():
	emit_signal("connection_succeeded")

	# Register ourselves with the server
	rpc_id(MultiplayerPeer.TARGET_PEER_SERVER, "register_player", my_name)


# Callback from SceneTree, called when server disconnect
func _server_disconnected():
	players.clear()
	emit_signal("server_disconnected")

	# Try to connect again
	connect_to_server()


# Callback from SceneTree, called when unabled to connect to server
func _connected_fail():
	get_tree().set_multiplayer_peer(null) # Remove peer
	emit_signal("connection_failed")

	# Try to connect again
	connect_to_server()


# Вызывается сервером, когда подключается новый игрок
# id              - ID подключенного игрока
# new_player_data - Имя (логин) игрока
@rpc func register_player(id, new_player_data):
	if new_player_data == my_name:
		my_id = id
	players[id] = new_player_data
	emit_signal("players_updated")


# Вызывается сервером, когда какой-либо игрок отключается
# id              - ID отключающегося игрока
@rpc func unregister_player(id):
	players.erase(id)
	emit_signal("players_updated")


# Вызывается сервером для доставки текстового сообщения пользователю
@rpc func messaging(message):
	emit_signal("message_received", message)


# Returns list of player names
func get_player_list():
	return players.values()


func send_command(command):
	rpc_id(MultiplayerPeer.TARGET_PEER_SERVER, "exec_command", command)
