import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateTextObject from './CreateTextObject';
import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';
import { getAllChildrens } from '../utils/GetAllChildrens';
type Options = {
  id: number;
  label: string;
  onClick: () => void;
};
const CreatePopupList = (
  scene: EndlessCanvas,
  x: number,
  y: number,
  options: Options[],
) => {
  const menu = scene.rexUI.add.menu({
    x,
    y,
    orientation: 'y',
    items: options,
    createButtonCallback: (item, i, options) => {
      return scene.rexUI.add.label({
        background: scene.rexUI.add.roundRectangle(0, 0, 2, 2, 0, COLOR_DARK),
        text: CreateTextObject(scene, item.label),
        space: {
          left: 10,
          right: 10,
          top: 10,
          bottom: 10,
          icon: 10,
        },
      });
    },
    easeIn: {
      duration: 500,
      orientation: 'y',
    },
    easeOut: {
      duration: 100,
      orientation: 'y',
    },
  });

  menu.on('button.click', (button: Phaser.GameObjects.Text) => {
    options.find((option) => option.label === button.text).onClick();

    menu.collapse();
  });
  scene.cameras.getCamera('ui').ignore(getAllChildrens(menu));

  return menu;
};
export default CreatePopupList;
