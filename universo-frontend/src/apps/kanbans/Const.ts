// Color
export const COLOR_LIGHT = 0x7b_5e_57;
export const COLOR_PRIMARY = 0x4e_34_2e;
export const COLOR_DARK = 0x26_0e_04;
export const COLOR_DRAG_STROKE = 0xff_00_00;
export const COLOR_BLACK = 0x00_00_00;
export const COLOR_WHITE = 0xff_ff_ff;
export const COLOR_BACKGROUND = 0xab_cd_ef;

// Depth
export const DragObjectDepth = 1000;
export const DefaultDepth = 0;

//Font Size
export const FONT_EXTRA_SMALL = '12px';
export const FONT_SMALL = '18px';
export const FONT_ORDINARY = '20px';
export const FONT_LARGE = '24px';
// export const FONT_HUGE = '32px';
